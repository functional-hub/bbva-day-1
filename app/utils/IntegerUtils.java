package utils;

import javax.annotation.Nullable;

public class IntegerUtils {

    @Nullable
    public static Integer intValueOf(String candidate) {
        try {
            return Integer.valueOf(candidate);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
