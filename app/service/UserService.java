package service;

import models.User;

import java.util.Arrays;
import java.util.List;

public class UserService {

    private User[] hardcodedUsers = {
        new User(1, "Bruce", false),
        new User(2, "Rachel", false),
        new User(3, "Brian", true),
        new User(4, "Susan", false),
        new User(5, "Louise", true),
        new User(6, "Deborah", false),
        new User(7, "Jennifer", false),
        new User(8, "Joyce", false),
        new User(9, "Julie", false),
        new User(10, "Lillian", true),
        new User(11, "Wanda", false),
        new User(12, "Phyllis", false),
        new User(13, "Justin", false),
        new User(14, "Christopher", true),
        new User(15, "Martha", true),
        new User(16, "Rachel", false),
        new User(17, "Shirley", true),
        new User(18, "Phyllis", false),
        new User(19, "Todd", false),
        new User(20, "Eric", true),
        new User(21, "Jonathan", false),
        new User(22, "Frank", true),
        new User(23, "Ernest", false),
        new User(24, "Christine", false),
        new User(25, "Helen", true),
        new User(26, "Albert", false),
        new User(27, "Carol", false),
        new User(28, "Harold", false),
        new User(29, "Arthur", false),
        new User(30, "Stephen", false)
    };

    public List<User> getAllUsers() {
        return Arrays.asList(hardcodedUsers);
    }

}
