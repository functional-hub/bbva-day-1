package service;

public class EvilService {

    private boolean active = false;

    private Thread thread = new Thread(() -> {
        active = true;
        while (active) {
            SharedUserService.user.setActive(false);
        }
    });

    public void start() {
        thread.start();
    }

    public void stop() {
        active = false;
    }

}
