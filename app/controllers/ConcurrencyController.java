package controllers;

import play.mvc.Result;
import play.mvc.Results;
import service.EvilService;

import javax.inject.Inject;

public class ConcurrencyController {

    private final EvilService evilService;

    @Inject
    public ConcurrencyController(EvilService evilService) {
        this.evilService = evilService;
    }

    public Result concurrency() {
        return Results.notAcceptable();
    }

}
