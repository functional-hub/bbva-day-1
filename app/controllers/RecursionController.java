package controllers;

import play.mvc.Result;

import static play.mvc.Results.notAcceptable;

public class RecursionController {

    public Result recursion() {
        return notAcceptable();
    }

}
