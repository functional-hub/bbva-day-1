package controllers;

import models.SessionRepository;
import play.mvc.Result;

import javax.inject.Inject;

import static play.mvc.Results.notAcceptable;

@SuppressWarnings("unused")
public class SessionController {

    private final SessionRepository repository;

    @Inject
    public SessionController(SessionRepository repository) {
        this.repository = repository;
    }

    public Result session() {
        return notAcceptable();
    }

    public Result toggleAnonymous() {
        return notAcceptable();
    }

    public Result togglePremium() {
        return notAcceptable();
    }

    public Result toggleBan() {
        return notAcceptable();
    }

}
