package controllers;

import service.IbanService;
import play.mvc.Result;
import play.mvc.Results;

import static play.mvc.Results.notAcceptable;

public class IbanController {

    // example ibans: "GB82 WEST 1234 5698 7654 32", "GB82 TEST 1234 5698 7654 32"
    public Result validateIban(String iban) {
        return notAcceptable();
    }

}
