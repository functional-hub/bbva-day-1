package controllers;

import play.mvc.Result;
import service.UserService;

import javax.inject.Inject;

import static play.mvc.Results.notAcceptable;

public class ImmutabilityController {

    private final UserService userService;

    @Inject
    public ImmutabilityController(UserService userService) {
        this.userService = userService;
    }

    public Result immutable() {
        return notAcceptable();
    }

}
