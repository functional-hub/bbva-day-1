package models;

import javax.inject.Singleton;

@Singleton
public class SessionRepository {

    private Session session = null;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }
}
